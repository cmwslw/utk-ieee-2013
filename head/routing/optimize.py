import random
import copy

class Block(object):
	'''A simple class for a block with a color and a height.'''

	def __init__(self, location, kind, color):
		self.location = location
		self.kind = kind
		self.color = color
	
	def __repr__(self):
		kind_list = ["ground", "rail", "air"]
		return "'" + kind_list[self.kind] + "_" + str(self.color) + "'"


class Cost(object):
	'''A base class for Cost functions.'''

	def get_cost(self, block_a, block_b):
		'''Should return cost to move blocks A and B into position.'''

		raise NotImplementedError("Should have implemented this")


class Chooser(object):
	'''Chooses sets of blocks to move.'''

	def __init__(self, blocks):
		self.blocks = blocks

	def pick_two(self):
		raise NotImplementedError("Should have implemented this")


class RandomChooser(Chooser):
	'''Chooses sets of blocks to move randomly.'''
	
	def pick_two(self):
		pick = []
		# Pop two random blocks
		pick.append(self.blocks.pop(random.randrange(len(self.blocks))))
		pick.append(self.blocks.pop(random.randrange(len(self.blocks))))

		return pick


class GroupChooser(Chooser):
	'''Chooses sets of blocks to move in sets of two.'''
	
	def pick_two(self):
		pick = []
		# Pop one random blocks
		pick.append(self.blocks.pop(random.randrange(len(self.blocks))))
		choices = []
		for block in self.blocks:
			if block.kind == pick[0].kind:
				choices.append(block)
		pick.append(choices[random.randrange(len(choices))])
		for block in self.blocks:
			if block.location == pick[1].location:
				self.blocks.remove(block)
				break

		return pick


def optimize(cost_class, blocks):
	'''Optimizes the path for a given initial row of blocks.'''

	# instantiate our cost class
	cost_object = cost_class()

	# Make sure we listed the right number of blocks
	assert len(blocks) == 14, "Block list must be of length 14"

	min_cost = 9999999
	min_order = None
	iteration = 0

	while True:
		this_cost = 0
		this_order = []
		chooser = GroupChooser(copy.copy(blocks))
		for i in range(7):
			pick = chooser.pick_two()
			this_cost += cost_object.get_cost(pick[0], pick[1])
			this_order.append(pick)
		if this_cost < min_cost:
			min_cost = this_cost
			min_order = this_order
			print "Found new cost at i=" + str(iteration) + ": " + str(min_cost)
			print min_order
		iteration += 1
	
	print total_cost

import optimize
import random

class TestCost(optimize.Cost):
	def get_cost(self, block_a, block_b):
		cost = 0
		if block_a.kind == block_b.kind:
			cost += 2
 		else:
			cost += 3
		if block_a.kind == 2 or block_b.kind == 2:
			cost += 5
		if abs(block_a.location - block_b.location) == 1:
			cost -= 1
		cost += abs(block_a.location - block_b.location) * 0.01
		
		return cost

blocks = [(0,0), (0,1), (0,2), (0,3), (0,4), (0,5), (1,0), (1,1), (1,2),
	(1,3), (1,4), (1,5), (2,0), (2,1)]

# Randomize blocks as a test
random.shuffle(blocks)

block_list = []
for i in range(len(blocks)):
	block_list.append(optimize.Block(i, blocks[i][0], blocks[i][1]))

print "Starting with arrangement:"
print block_list

optimize.optimize(TestCost, block_list)

#!/usr/bin/python

"""
color_profile.py: Find the color profile of blocks. POC.

Usage: ./color_profile.py blocks.jpg

Dependencies: OpenCV, python-colormath
"""

import getopt, sys
import cv
from imaging.blocks import BlockFinder 

__author__      = "Cory Walker"
__email__       = "cwalker32@gmail.com"
__status__      = "Prototype"

def get_profile(in_image, out_image, display, width, height):  
    # Ready set go
    im = cv.LoadImageM(in_image, 1)
    im_resized = cv.CreateMat(height, width, cv.CV_8UC3)
    im_debug = cv.CreateMat(height, width, cv.CV_8UC3)
    #im_resized = cv.CreateMat(800, 600, cv.CV_8UC3)
    #im_debug = cv.CreateMat(800, 600, cv.CV_8UC3)
    cv.Resize(im, im_resized)
    cv.Resize(im, im_debug)
    bf = BlockFinder(im_resized, int(im_resized.rows/2), im_debug)
    profile = bf.get_profile()
        
    if(out_image):
        cv.SaveImage(out_image, im_debug)

    if(display):
        cv.NamedWindow('display')
        cv.MoveWindow('display', 10, 10)
        cv.ShowImage('display', im_debug)

        # Listen for ESC key
        while True:
            c = cv.WaitKey(7) % 0x100
            if c == 27:
                break

    return profile

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:d", ["help", "in=", "out=", "width=", "height="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    in_image = None
    out_image = None
    display = False
    width = 800
    height = 240
    for o, a in opts:
        if o == "-d":
            display = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-i", "--in"):
            in_image = a
        elif o in ("-o", "--out"):
            out_image = a
        elif o in ("--width"):
            width = int(a)
        elif o in ("--height"):
            height = int(a)
        else:
            assert False, "unhandled option"
    
    if in_image == None:
        print "Error: you must specify an input image with --in=<IMAGE>"
        sys.exit()

    print get_profile(in_image, out_image, display, width, height)

if __name__ == "__main__":
    main()



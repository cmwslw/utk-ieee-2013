import random

colors = ['yellow', 'green', 'brown', 'red', 'orange', 'blue']
air_colors = ['yellow', 'orange']
types = ['rail', 'sea', 'air']

blocks = []
for t in types:
    if t == 'air':
        type_colors = air_colors
    else:
        type_colors = colors
    for color in type_colors:
        new_block = color + ' ' + t
        blocks.append(new_block)

random.shuffle(blocks)

print blocks

for i, block in enumerate(blocks):
    print i, block

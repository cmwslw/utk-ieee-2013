#!/usr/bin/python

"""
run_tests.py: Assert that all of the training images can be parsed correctly.

Usage: ./run_tests.py

Dependencies: OpenCV, python-colormath
"""

import getopt, sys, os, time
import cv
from head.index_blocks import index_blocks_spawn
from imaging.training import training_images

__author__      = "Cory Walker"
__email__       = "cwalker32@gmail.com"
__status__      = "Prototype"

lengths = {}

def test_image(image, width, height):
    print "Processing '%s'..." % image[0]
    bf = index_blocks_spawn(False, image[1], width, height, image[0], None)

    if os.path.exists(image[2]):
        with open(image[2]) as f:
            expected = []
            actual = []
            for line in f.readlines():
                expected.append(line.strip().split(' '))
            for block in bf.blocks:
                actual.append([block.color, block.get_type(), block.height])
            for i in range(len(expected)):
                if expected[i][0] != 'black':
                    if expected[i][1] not in lengths:
                        lengths[expected[i][1]] = [actual[i][2]]
                    else:
                        lengths[expected[i][1]].append(actual[i][2])
        return True
    else:
        print "ASSERT FILE NOT FOUND! MARKED AS FAILURE."
        return False

def run_tests(width, height):
    print "Testing %d images at %dx%d..." % (len(training_images), width, height)
    print "----------------------------------"

    start_time = time.time()
    successes = 0
    failures = 0
    
    for image in training_images:
        if test_image(image, width, height):
            successes += 1
        else:
            failures += 1

    elapsed_time = time.time() - start_time

    print "----------------------------------"
    print "Took %f seconds (%f FPS)" % (elapsed_time, len(training_images) / elapsed_time)
    print "Finished with %d successes and %d failures." % (successes, failures)
    if failures == 0:
        print "PASSED"
    else:
        print "FAILED"

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "", ["width=", "height="])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    width = 320
    height = 240
    for o, a in opts:
        if o in ("--width"):
            width = int(a)
        elif o in ("--height"):
            height = int(a)
        else:
            assert False, "unhandled option"
    
    run_tests(width, height)
    for key, value in lengths.items():
        print key, sorted(value)

if __name__ == "__main__":
    main()



#!/usr/bin/python

"""
find_lines.py: Find the locations and directions of white lines

Usage: ./find_lines.py blocks.jpg

Dependencies: OpenCV, python-colormath
"""

import getopt, sys
import time, datetime
import pickle
import os
import cv
from imaging.lines import LineFinder
import cProfile

__author__      = "Cory Walker"
__email__       = "cwalker32@gmail.com"
__status__      = "Prototype"

start_time = time.time()
folder = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + '/'

def current_lines(drop_off, save, width=320, height=240):
    data = ('webcam', width, height)
    command = 'find_lines.py --in=%s --width=%d --height=%d -p' % data
    if save:
        base = '/home/utk-ieee/Code/ieeelog/'
        if not os.path.exists(base + folder):
            os.makedirs(base + folder)
        fn_base = base + folder + str(int((time.time() - start_time)*1000))
        fn = fn_base + '.png'
        print fn
        command += ' --out=%s --origout=%s' % (fn, fn_base + '_orig.png')
    if drop_off:
        command += ' --dropoff'
    out = os.popen(command).read()
    lf = pickle.load(open('/home/utk-ieee/lf.p', 'rb'))
    #lf = find_lines("webcam", fn, False, width, height, fn_base + '_orig.png')
    return lf

def find_lines(in_image, out_image, display, width, height, drop_off, orig_out=None):
    # Ready set go
    if in_image == 'webcam':
        capture = cv.CaptureFromCAM(1)
        for i in range(3):
            im = cv.QueryFrame(capture)
        if orig_out:
            cv.SaveImage(orig_out, im)
    else:
        im = cv.LoadImageM(in_image, 1)
    im_resized = cv.CreateMat(height, width, cv.CV_8UC3)
    im_debug = cv.CreateMat(height, width, cv.CV_8UC3)
    #im_resized = cv.CreateMat(800, 600, cv.CV_8UC3)
    #im_debug = cv.CreateMat(800, 600, cv.CV_8UC3)
    cv.Resize(im, im_resized)
    cv.Resize(im, im_debug)
    lf = LineFinder(im_resized, im_debug, drop_off)
    lines = lf.get_lines()
        
    if(out_image):
        cv.SaveImage(out_image, im_debug)

    if(display):
        cv.NamedWindow('display')
        cv.MoveWindow('display', 10, 10)
        cv.ShowImage('display', im_debug)

        # Listen for ESC key
        while True:
            c = cv.WaitKey(7) % 0x100
            if c == 27:
                break

    return lf

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:dp", ["help", "in=", "out=", "origout=", "width=", "height=", "dropoff"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    in_image = None
    out_image = None
    orig_out = None
    drop_off = False
    display = False
    to_pickle = False
    width = 320
    height = 240
    for o, a in opts:
        if o == "-d":
            display = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-i", "--in"):
            in_image = a
        elif o in ("-o", "--out"):
            out_image = a
        elif o in ("--origout"):
            orig_out = a
        elif o in ("--dropoff"):
            drop_off = True
        elif o in ("--width"):
            width = int(a)
        elif o in ("--height"):
            height = int(a)
        elif o in ("-p"):
            to_pickle = True
        else:
            assert False, "unhandled option"
    
    if in_image == None:
        print "Error: you must specify an input image with --in=<IMAGE>"
        sys.exit()

    #cProfile.run("find_lines('training/sequence_new/image-5.jpg', None, False, 640, 480)")
    lf = find_lines(in_image, out_image, display, width, height, drop_off, orig_out)
    if to_pickle:
        pickle.dump(lf, open('/home/utk-ieee/lf.p', 'wb'), protocol=2)
    else:
        for line in lf.lines:
            print line

if __name__ == "__main__":
    main()



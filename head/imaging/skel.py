import cv
import random

def show_image(debug_image):
    win_str = str(random.randint(0,10000000))
    cv.NamedWindow(win_str)
    cv.MoveWindow(win_str, 10, 10)
    cv.ShowImage(win_str, debug_image)

def skel(img):
    eroded = cv.CreateMat(img.rows, img.cols, cv.CV_8UC1)
    temp = cv.CreateMat(img.rows, img.cols, cv.CV_8UC1)
    skel = cv.CreateMat(img.rows, img.cols, cv.CV_8UC1)
    
    done = False
     
    while( not done):
        cv.Erode(img, eroded)
        cv.Dilate(eroded, temp)
        cv.Sub(img,temp, temp)
        cv.Or(skel,temp,skel)
        img = cv.CloneMat(eroded)
     
        if cv.CountNonZero(img) == 0:
            done = True
    show_image(skel)
    return skel

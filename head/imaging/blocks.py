"""
blocks.py: Tries to determine the order of blocks in a given row in an image.
"""

import color_proc
import cv
from lines import LineFinder, line_intersection

__author__      = "Cory Walker"
__email__       = "cwalker32@gmail.com"

def binarize(colors, desired_color):
    filtered = []
    for color in colors:
        filtered.append(color[0] == desired_color)
    return filtered

def dilate(l, radius):
    dilated = []
    for i in range(len(l)):
        low = max(i-radius, 0)
        high = min(i+radius, len(l)-1)
        for j in range(low, high):
            if l[j]:
                dilated.append(True)
                break
        else:
            dilated.append(False)
    return dilated

def erode(l, radius):
    eroded = []
    for i in range(len(l)):
        low = max(i-radius, 0)
        high = min(i+radius, len(l)-1)
        for j in range(low, high):
            if not l[j]:
                eroded.append(False)
                break
        else:
            eroded.append(True)
    return eroded

def draw_rect(im, pt1, pt2, color):
    r = 2
    cv.Rectangle(im, pt1, pt2, cv.CV_RGB(100,100,100), r)
    cv.Rectangle(im, (pt1[0]+r, pt1[1]+r), (pt2[0]-r, pt2[1]-r), color, -1)

class Block:
    def __init__(self, color, moment):
        self.color = color
        self.moment = moment
        self.height = None
        self.borders = None

    def __str__(self):
        return repr([self.color, self.get_type(), self.moment, self.height, self.borders])

    def get_type(self):
        if self.height == None:
            return None
        if self.height > 72:
            return 'rail'
        elif self.height > 45:
            return 'sea'
        else:
            return 'air'

class BlockFinder:
    def __init__(self, im, row, im_debug, drop_off=False):
        self.im = im
        self.im_cols = im.cols
        self.im_rows = im.rows
        self.im_debug = im_debug
        self.row = row
        self.drop_off = drop_off

    def __getstate__(self):
        odict = self.__dict__.copy() # copy the dict since we change it
        # Delete the CVmats since they cannot be pickled
        del odict['im']
        del odict['im_debug']
        return odict

    def lines_to_slots(self):
        if len(self.lines) < 2:
            raise Exception('Only %s white lines detected. Something\'s probably wrong.' % len(self.lines))
        slots = []
        for i in range(0, len(self.lines)-1):
            slots.append((self.lines[i][1], self.lines[i+1][1]))
        return slots

    def remove_small_slots(self, minimum):
        filtered = []
        for slot in self.slots:
            if (slot[1] - slot[0]) >= minimum:
                filtered.append(slot)
        return filtered

    def histogram_range(self, colors, start, end):
        hist = {}
        for key in color_proc.colors.keys():
            hist[key] = 0
        for i in range(start, end):
            hist[colors[i][0]] += 1
        return hist

    def most_popular_color(self, histogram):
        most_color = None
        with_count = -1;
        for color in histogram.keys():
            # Skip the white color of the line
            if color == 'white':
                pass
            elif histogram[color] > with_count:
                most_color = color
                with_count = histogram[color]
        return most_color

    def find_color_moment(self, colors, desired_color, slot):
        x_total = 0
        total_pixels = 0
        for x in range(slot[0], slot[1]):
            if colors[x][0] == desired_color:
                x_total += x
                total_pixels += 1

        # Hopefully no divide by zero!!
        if total_pixels == 0:
            return 0
        else:
            return x_total/total_pixels

    def find_vert_borders(self, block, y):
        top = color_proc.find_vertical_tranny_point(self.im, block.moment, y, block.color, -2, 5)
        bottom = color_proc.find_vertical_tranny_point(self.im, block.moment, y, block.color, 2, 5)
        return (top, bottom)

    def get_blocks(self):
        # Draw scan line
        #cv.Line(im_debug, (0,self.row), (800,self.row), cv.CV_RGB(60,60,60), 2)

        debug = False
        if debug: print "Finding white lines"
        # Locate white lines
        self.lf = LineFinder(cv.CloneMat(self.im), self.im_debug, self.drop_off)
        self.lf.get_lines()
        if self.lf.slotwidth_avg:
            self.row = self.lf.intersect_y_avg - int(self.lf.slotwidth_avg*0.8)
        else:
            self.row = int(self.im.rows/2)
 
        if debug: print "Getting colors"
        self.colors = color_proc.get_colors(self.im, self.row)
        if debug: print "Finding intersect locations"
        p0 = (-1000, self.row)
        p1 = (1000, self.row)
        self.lines = []
        for line in self.lf.lines:
            intersect = line_intersection(p0, p1, line.pt1, line.pt2)
            x = intersect[0]
            new_line = (x-10, x, x+10, 20)
            self.lines.append(new_line)
        for line in self.lines:
            draw_rect(self.im_debug, (line[0], self.row-5), (line[2], self.row+5), cv.CV_RGB(255,255,255))

        # Find slot locations between white 'lines'
        self.slots = self.lines_to_slots()
        self.slots = self.remove_small_slots(self.im.cols/9)

        if debug: print "Counting colors"
        # Count the different colors between those lines
        self.histograms = []
        for slot in self.slots:
            self.histograms.append(self.histogram_range(self.colors, slot[0], slot[1]))

        # Find the most prevalent color between the white lines
        self.color_orders = []
        for hist in self.histograms:
            self.color_orders.append(self.most_popular_color(hist))

        # Append to self.blocks
        self.blocks = []
        for i in range(len(self.color_orders)):
            moment = self.find_color_moment(self.colors, self.color_orders[i], self.slots[i])
            self.blocks.append(Block(self.color_orders[i], moment))

        if debug: print "Finding heights"
        # Find block heights
        for block in self.blocks:
            if block.color != 'black':
                if self.drop_off:
                    borders = (80, 140)
                else:
                    borders = self.find_vert_borders(block, self.row)
                if (borders[0] == None or borders[1] == None):
                    block.height = None
                else:
                    block.height = borders[1]-borders[0]
                block.borders = borders

        # Draw block rectangles
        for block in self.blocks:
            if block.color == 'black':
                top = self.row-50
                bottom = self.row+50
            else:
                if block.borders[0] == None:
                    top = 0
                else:
                    top = block.borders[0]
                if block.borders[1] == None:
                    top = self.im.rows
                else:
                    bottom = block.borders[1]
            draw_rect(self.im_debug, (block.moment-10, top), (block.moment+10, bottom), cv.CV_RGB(*color_proc.colors[block.color][0]))

        spacing = 32
        start_x = self.im.cols - len(self.blocks) * spacing
        start_y = self.im.rows - spacing
        for i in range(len(self.blocks)):
            draw_rect(self.im_debug, (start_x+spacing*i, start_y), (start_x+spacing*i+24, self.im.rows-8), cv.CV_RGB(*color_proc.colors[self.blocks[i].color][0]))

        return self.blocks


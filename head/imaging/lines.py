"""
lines.py: Tries to determine the order of lines in an image.
"""

from math import cos, sin, pi, atan2
# Third party
import cv
# Local
import color_proc
from skel import skel


__author__      = "Cory Walker"
__email__       = "cwalker32@gmail.com"

def draw_rect(im, pt1, pt2, color):
    r = 2
    cv.Rectangle(im, pt1, pt2, cv.CV_RGB(100,100,100), r)
    cv.Rectangle(im, (pt1[0]+r, pt1[1]+r), (pt2[0]-r, pt2[1]-r), color, -1)
'''
def angle_dist(a, b):
    dist = abs(a - b) % 2*pi
    if dist > pi:
        dist = 2*pi - dist
    return dist
'''
def angle_dist(a, b):
    return abs((a+pi-b)%(2*pi) - pi)

def line_intersection(p1, p2, p3, p4):
    x1, y1 = p1
    x2, y2 = p2
    x3, y3 = p3
    x4, y4 = p4
    x1 = float(x1)
    x2 = float(x2)
    x3 = float(x3)
    x4 = float(x4)
    y1 = float(y1)
    y2 = float(y2)
    y3 = float(y3)
    y4 = float(y4)
    px=((x1*y2-y1*x2)*(x3-x4)-(x1-x2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
    py=((x1*y2-y1*x2)*(y3-y4)-(y1-y2)*(x3*y4-y3*x4))/((x1-x2)*(y3-y4)-(y1-y2)*(x3-x4))
    return (int(px), int(py))

def get_avg_line(lines):
    tot_rho = 0
    tot_x = 0
    tot_y = 0
    for line in lines:
        tot_rho += line.rho
        tot_x += cos(line.theta*2)
        tot_y += sin(line.theta*2)
    avg_rho = tot_rho / len(lines)
    avg_x = tot_x / len(lines)
    avg_y = tot_y / len(lines)
    avg_theta = atan2(avg_y, avg_x) / 2
    if avg_theta < 0:
        avg_theta += pi
    if avg_theta >= .75 * pi:
        avg_rho *= -1
    return Line(avg_rho, avg_theta)

class Line:
    def __init__(self, rho, theta):
        self.rho = rho
        self.theta = theta
        self.pt1 = None
        self.pt2 = None
        self.intersect = None
        # 0 = no, 1 = left, 2 = right
        self.end = 0

    def __str__(self):
        return 'rho: %f, theta: %f' % (self.rho, self.theta)

    def is_horizontal(self):
        return (pi/2 * 0.8) < self.theta < (pi/2 * 1.2)

    def calculate_points(self):
        a = cos(self.theta)
        b = sin(self.theta)
        x0 = a * self.rho
        y0 = b * self.rho
        self.pt1 = (cv.Round(x0 + 1000*(-b)), cv.Round(y0 + 1000*(a)))
        self.pt2 = (cv.Round(x0 - 1000*(-b)), cv.Round(y0 - 1000*(a)))

    def calc_baseline_intersect(self, baseline):
        self.intersect = line_intersection(self.pt1, self.pt2, baseline.pt1, baseline.pt2)

    def dist(self, otherline, im_cols):
        if self.rho < 0:
            self.rho *= -1
        dist = angle_dist(self.theta*2, otherline.theta*2) * 15
        dist += abs(self.rho - otherline.rho)*246/im_cols
        return dist
        
 
class LineFinder:
    def __init__(self, im, im_debug, drop_off=False):
        self.im = im
        self.im_cols = im.cols
        self.im_rows = im.rows
        self.im_debug = im_debug
        self.baseline = None
        self.all_lines = []
        self.more_lines = []
        self.lines = []
        self.intersect_y_avg = None
        self.slotwidth_avg = None
        self.drop_off = drop_off

    def __getstate__(self):
        odict = self.__dict__.copy() # copy the dict since we change it
        del odict['im']
        del odict['im_debug']
        return odict

    def apply_to_members(self, lines):
        possible_baselines = []
        for line in lines:
            if line.is_horizontal():
                possible_baselines.append(line)
        if len(possible_baselines):
            min_dist = 99999
            most_likely = None
            for line in possible_baselines:
                dist = abs(self.im.rows/2 - line.rho)
                if dist < min_dist:
                    min_dist = dist
                    most_likely = line
            self.baseline = most_likely
        for line in lines:
            line.calculate_points()
            if self.baseline == None or line != self.baseline:
                if not line.is_horizontal():
                    self.lines.append(line)
        self.lines = sorted(self.lines, key=lambda line: abs(line.rho))
        if self.baseline:
            for line in self.lines:
                line.calc_baseline_intersect(self.baseline)
        '''
        if len(self.lines) > 1:
            crit_dist = self.im.cols / 5.3
            for i in range(len(self.lines)):
                end = 0
                if i == 0:
                    if self.lines[i].intersect[0] > crit_dist:
                        end = 1
                if i == len(self.lines)-1:
                    if self.lines[i].intersect[0] < self.im.cols - crit_dist:
                        end = 2
                self.lines[i].end = end
        '''
 

    def simplify_lines(self):
        similar_groups = []
        # Clone the list since we will be deleting it
        hlines = list(self.all_lines)
        while len(hlines):
            curr = hlines[0]
            similar = []
            for line in reversed(hlines):
                dist = line.dist(curr, self.im.cols)
                # This value is a constant we can change
                if dist < 28:
                    similar.append(line)
                    hlines.remove(line)
            similar_groups.append(similar)
        simp_lines = []
        for lines in similar_groups:
            avg_line = get_avg_line(lines)
            simp_lines.append(avg_line)
        return simp_lines

    def simplify_more_lines(self):
        more_simp_lines = []
        if len(self.lines) > 1:
            first = self.lines[0]
            last = self.lines[-1]
            width = self.slotwidth_avg*.85
            potential_lines = []
            potential_lines.append(Line(first.rho-width, first.theta))
            if last.rho < 0:
                potential_lines.append(Line(last.rho-width, last.theta))
            else:
                potential_lines.append(Line(last.rho+width, last.theta))
            similar_groups = []
            for potential_line in potential_lines:
                similar_group = []
                for line in self.more_lines:
                    if line.dist(potential_line, self.im.cols) < 20:
                        similar_group.append(line)
                similar_groups.append(similar_group)
            for lines in similar_groups:
                if len(lines):
                    avg_line = get_avg_line(lines)
                    avg_line.calculate_points()
                    avg_line.calc_baseline_intersect(self.baseline)
                    more_simp_lines.append(avg_line)
        return more_simp_lines

    def calc_averages(self):
        if len(self.lines):
            self.intersect_y_avg = 0
            for line in self.lines:
                self.intersect_y_avg += line.intersect[1]
            self.intersect_y_avg /= len(self.lines)
        if len(self.lines) > 1:
            self.slotwidth_avg = 0
            for i in range(len(self.lines)-1):
                self.slotwidth_avg += self.lines[i+1].intersect[0] - self.lines[i].intersect[0]
            self.slotwidth_avg /= len(self.lines)-1

    def find_baseline_info(self):
        if self.baseline:
            center_x = self.im.cols/2
            center = Line(center_x, 0)
            center.calculate_points()
            intersect = line_intersection(self.baseline.pt1, self.baseline.pt2, center.pt1, center.pt2)
            self.dist_to_baseline = self.im.rows - intersect[1]
            self.baseline_theta = (self.baseline.theta - pi/2) * (180/pi)
        else:
            self.dist_to_baseline = None
            self.baseline_theta = None

    def get_lines(self):
        # Generate skeleton
        if self.drop_off:
            # Drop off zone tends to reflect more, and there are no shadows from blocks
            # Because of this, set threshold higher
            thresh = 90
        else:
            thresh = 75
        cv.Threshold(self.im, self.im, thresh, 255, 0)
        #show_image(self.im)
        gray = cv.CreateMat(self.im.rows, self.im.cols, cv.CV_8UC1)
        cv.CvtColor(self.im, gray, cv.CV_RGB2GRAY)
        cv.Threshold(gray, gray, 250, 255, 0)
        gray = skel(gray)

        # Find Hough Lines
        storage = cv.CreateMemStorage(0)
        #thresh = self.im.cols/8
        thresh = int(20+.0625*self.im.cols)
        hlines = cv.HoughLines2( gray, storage, cv.CV_HOUGH_STANDARD, 1, cv.CV_PI/180, thresh, 0, 0 )
        for line in hlines:
            self.all_lines.append(Line(line[0], line[1]))

        simplified = self.simplify_lines()
        self.apply_to_members(simplified)
        self.calc_averages()
        # This should get rid of the stray lines we get way off to the left from other zones
        if len(self.lines) > 1: # To avoid no slotwidth errors
            scaled_avg = self.slotwidth_avg/float(self.im.cols)
            if scaled_avg > 0.27:
                # Delete first line
                self.lines = sorted(self.lines, key=lambda line: abs(line.rho))
                self.lines.pop(0)
                # Recalculate averages
                self.calc_averages()

        thresh2 = int((20+.0625*self.im.cols)/2)
        hlines = cv.HoughLines2( gray, storage, cv.CV_HOUGH_STANDARD, 1, cv.CV_PI/180, thresh2, 0, 0 )
        del(storage)
        for line in hlines:
            self.more_lines.append(Line(line[0], line[1]))
        self.lines.extend(self.simplify_more_lines())
        self.lines = sorted(self.lines, key=lambda line: abs(line.rho))

        self.find_baseline_info()

        # Display on im_debug
        for line in self.all_lines:
            line.calculate_points()
            cv.Line( self.im_debug, line.pt1, line.pt2, cv.RGB(127,0,0), 1, 8 )

        if self.im.cols < 200:
            fatline = 1
        else:
            fatline = 2

        if self.baseline:
            cv.Line(self.im_debug, self.baseline.pt1, self.baseline.pt2, cv.RGB(0, 255, 0), fatline, 8)
        for line in self.lines:
            cv.Line(self.im_debug, line.pt1, line.pt2, cv.RGB(255, 0, 0), fatline, 8)
            if line.end == 0:
                color = cv.RGB(0, 0, 255)
            else:
                color = cv.RGB(255, 160, 0)
            cv.Circle(self.im_debug, line.intersect, 5, color, -1)

        return (self.baseline, self.lines)


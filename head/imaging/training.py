#locations = [
#    ('training/sequence/image-%d.jpg', 'training/sequence/out/image-%d.jpg', 'training/sequence/image-%d.assert', 6),
#]
#locations = [
#    ('training/sequence_new/image-%d.jpg', 'training/sequence_new/out/image-%d.jpg', 'training/sequence_new/image-%d.assert', 9),
#]
locations = [
    ('training/comp/image-%d.jpg', 'training/comp/out/image-%d.jpg', 'training/comp/image-%d.assert', 12),
]


training_images = []
for location, output_loc, assert_loc, number in locations:
    for i in range(number):
        training_images.append((location % i, output_loc % i, assert_loc % i))

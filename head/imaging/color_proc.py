"""
colors.py: Find the color components of a row in an image
"""

from colormath.color_objects import RGBColor
from math import pow, sqrt

__author__      = "Cory Walker"
__email__       = "cwalker32@gmail.com"

colors = {
        'black': [(16, 6, 4), (39, 18, 7)],
        'white': [(254, 248, 107),],
        'orange': [(238, 86, 48),],
        'yellow': [(253, 247, 51),],
        'blue': [(80, 69, 64), (54, 51, 48)],
        'red': [(203, 58, 39),],
        'green': [(54, 35, 13), (29, 21, 6)],
        'brown': [(87, 32, 18), (41, 11, 5)]
    }
'''
colors = {
        'black': [(46, 23, 12),],
        'white': [(254, 248, 107),],
        'orange': [(238, 86, 48),],
        'yellow': [(253, 247, 51),],
        'blue': [(80, 69, 64),],
        'red': [(203, 58, 39),],
        'green': [(54, 35, 13),],
        'brown': [(82, 35, 15),]
    }
'''
'''
colors = {
        'black': [(38, 24, 14),],
        'white': [(254, 249, 148),],
        'orange': [(238, 85, 48),],
        'yellow': [(253, 247, 51),],
        'blue': [(47, 59, 71),],
        'red': [(160, 44, 29),],
        'green': [(39, 33, 14),],
        'brown': [(47, 19, 9), (36, 10, 3)]
    }
'''
official_colors = {
        'black': (0, 0, 0),
        'white': (255, 255, 255),
        'orange': (245, 163, 49),
        'yellow': (252, 247, 54),
        'blue': (0, 68, 234),
        'red': (237, 71, 48)
    }

def color_distance(sample, standard):
    rgb_samp = RGBColor(sample[0], sample[1], sample[2])
    rgb_std = RGBColor(standard[0], standard[1], standard[2])
    lab_samp = rgb_samp.convert_to('lab')
    lab_std = rgb_std.convert_to('lab')
    return lab_std.delta_e(lab_samp)
    '''
    r_dist = pow(standard[0]-sample[0], 2)
    g_dist = pow(standard[1]-sample[1], 2)
    b_dist = pow(standard[2]-sample[2], 2)
    return sqrt(r_dist+g_dist+b_dist)
    '''

def bgr_to_rgb(bgr):
    return (bgr[2], bgr[1], bgr[0])

def color_at(im, x, y):
    min_dist = 9999
    best_color = "unknown"
    for color_str in colors.keys():
        for color in colors[color_str]:
            dist = color_distance(bgr_to_rgb(im[y,x]), color)
            if dist < min_dist:
                min_dist = dist
                best_color = color_str
    return (best_color, min_dist)
 
def get_colors(im, row):
    my_colors = []
    granularity = 4
    for col in range(0, im.cols, granularity):
        color = color_at(im, col, row)
        for i in range(granularity):
            my_colors.append(color)
    return my_colors

def find_vertical_tranny_point(im, x, y, color, step, max_failures):
    failures = 0
    while failures < max_failures:
        y += step
        if y < 0:
            return None
        if y >= im.rows:
            return None
        if color_at(im, x, y)[0] == color:
            failures == 0
        else:
            failures += 1
    return y - step * max_failures

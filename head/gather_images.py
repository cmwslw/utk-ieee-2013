#!/usr/bin/python

"""
index_blocks.py: Find the order and color of blocks. POC.

Usage: ./index_blocks.py blocks.jpg

Dependencies: OpenCV, python-colormath
"""

import getopt, sys
import cv

__author__      = "Cory Walker"
__email__       = "cwalker32@gmail.com"
__status__      = "Prototype"

def main():
    capture = cv.CaptureFromCAM(1)
    for i in range(3):
        im = cv.QueryFrame(capture)
    print "Ready to snap pictures..."
    inp = ''
    i = 0
    while inp != "d":
        print "Enter to snap, 'd' for done: ",
        inp = raw_input()
        if inp != "d":
            del capture
            fn = 'training/image-%d.jpg' % i
            capture = cv.CaptureFromCAM(1)
            for x in range(3):
                im = cv.QueryFrame(capture)
            cv.SaveImage(fn, cv.QueryFrame(capture))
            print fn
        i += 1
 

if __name__ == "__main__":
    main()



# Python modules
import time

from head.spine import pixelmove
from head.find_lines import current_lines

# We will mostly be using the following functions:
# s.move(speed, direction, angular_speed)
# speed: 0-1, direction: -180 to 180 (0 straight), angular_speed: -1 to 1
# speed and angular speed can be floating point, direction must be an integer
# time.sleep(seconds)
# seconds can be floating point
# Examples:
# Move forward full speed but 18 degrees to the right (I think):
# s.move(1, 18, 0) # no angular speed
# Move backward full speed with an angular speed of -1 (maximum):
# s.move(1, 180, -1)
# Sleep for 1.3 seconds:
# time.sleep(1.3)
# Stop all motors:
# s.move(0, 0, 0)

#Pro Tip: More Control = More Time
#More Power = Less Control (Motor Power)

#Nother Pro Tip: For a more controlled rotation
#apply more energy to the angular speed and less to the regular speed

def to_zone(s, curr_zone, dest_zone):
    if curr_zone == 0 and dest_zone == 1:
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Don't mess with this. It works for now :P
        s.move(.75, -45, 0)
        time.sleep(1.5)
        s.move(.75, 0, 0)
        time.sleep(1.2)
        s.move(0,0,0)
    elif curr_zone == 1 and dest_zone == 3:
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Movement functions here
        s.move(0.75,150,0) #Back up, spin
        time.sleep(0.75)
        s.move(0.5,0,0.75)
        time.sleep(0.8)
        s.move(0,0,0)
    elif curr_zone == 3 and dest_zone == 1:
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Movement functions here
        s.move(1,-135,-.5) #Back up, spin
        time.sleep(1)
        s.move(0,0,0)
    elif curr_zone == 1 and dest_zone == 4:
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Movement functions here
        s.move(1,-120, -0.5) 
        time.sleep(1.5)
        s.move(0,0,0)
    elif curr_zone == 4 and dest_zone == 1: # works
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Movement functions here
        s.move(.9,-110,0)
        time.sleep(2.5)
        s.move(0,0,-.7)
        time.sleep(1.8)
        s.move(.4, 0, 0)
        time.sleep(1)
        s.move(0,0,0)
    elif curr_zone == 2 and dest_zone == 3:
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Movement functions here
        s.move(1,0,-0.5)
        time.sleep(0.5)
        s.move(1,0,0)
        time.sleep(1.5)
        s.move(0,0,0)
    elif curr_zone == 3 and dest_zone == 2:
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Movement functions here
        s.move(1,0,-0.5)
        time.sleep(0.25)
        s.move(1,-90,0)
        time.sleep(1.5)
        s.move(0,0,0)
    elif curr_zone == 2 and dest_zone == 4: # works
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Movement functions here
        s.move(.9,-145,0)
        time.sleep(1.1)
        s.move(0,0,1)
        time.sleep(1)
        s.move(0.5,0,.3)
        time.sleep(.6)
        s.move(0,0,0)
    elif curr_zone == 4 and dest_zone == 2:
        print "Moving from %d to %d..." % (curr_zone, dest_zone)
        # Movement functions here
        s.move(1,0,-0.5)
        time.sleep(0.25)
        s.move(1,0,0)
        time.sleep(1)
        s.move(0,0,0)
    else:
        print "We do not need to worry about this combination now."
    return dest_zone

def x_slide(s, offset, dest): #offset in inches from leftmost line in zone 1
    const = 12.5

#back up ~3 inches to avoid hitting blocks
    s.move(1,180,0)
    time.sleep(0.4)
    s.move(0,0,0)

    if dest == 3:
#move to left extent of zone 1
        s.move(1,90,0)
        if offset > 0: time.sleep(max(offset / const - .3, 0))
        s.move(0,0,0)
#rotate to face zone 3
        s.move(0,0,1)
        time.sleep(0.60)
        s.move(0,0,0)
#move forward ~1 inches
        s.move(1,0,0)
        time.sleep(0.13)
        s.move(0,0,0)
    elif dest == 4:
#move to right extent of zone 2
        s.move(1,-90,0)
        time_to_sleep = (42.5 - offset ) / const
        if time_to_sleep < 0: time_to_sleep = 0
        time.sleep(time_to_sleep)
        s.move(0,0,0)
#rotate to face zone 4
        s.move(0,0,-1)
        time.sleep(1.25)
        s.move(0,0,0)
#move forward ~2 inches
        s.move(1,0,0)
        time.sleep(0.6)
        s.move(0,0,0)
    else:
        print "Err: Dest not 3 or 4."
    return dest

def x_return(s, offset, src, dest): #offset in inches from leftmost edge in zone src [3 or 4].  
#Dest = 1 moves to left extent of zone 1.  Dest = 2 moves to right extent of zone 2
    target_3 = 8.0 #offset sought to before returning from zone 3
    target_4 = 8.0 #offset sought to before returning from zone 4
    const = 13.0

    # This causes problems when src == 3 and offset > target_3
    if not (src == 3 and offset > target_3):
        # Back off a bit
        s.move_for(0.45,.7,180,0)

    if src == 4: # rail
        #move to offset target_4
        if offset > target_4:
            #move left to offset
            s.move(1,90,0)
            time.sleep((offset - target_4) / const)
            s.move(0,0,0)
        else: #offset < target_4
            #move right to offset
            s.move(1,-90,0)
            time.sleep((target_4 - offset) / const)
            s.move(0,0,0)

        time.sleep(0.1)
        #rotate to face zone 2
        s.move(0,0,1)
        # Used to be 1.22 with full battery:
        time.sleep(1.27)
        s.move(0,0,0)
        time.sleep(0.1)
        #move forward ~8 inches
        s.move(1,0,0)
        time.sleep(0.8)
        s.move(0,0,0)
        lf = current_lines(False, True)
        #pixelmove.move_to_last_line(s, lf)
        pixelmove.correct_baseline_error(s, lf)
        print "corrected baseline error when moving from rail!!"

        if dest == 1:
            time.sleep(0.4)
            #move to left extent of zone 1
            s.move(1,90,0)
            time.sleep(42.5 / const)
            s.move(0,0,0)

    elif src == 3: # sea
        #move to offset target_3
        if offset > target_3:
            #move left to offset
            s.move(1,90,0)
            time.sleep((offset - target_3) / const)
            s.move(0,0,0)
            # This is the special case to back up after we're out of the way of blocks:
            time.sleep(0.1)
            # Back off a bit
            s.move_for(0.45,.7,180,0)
        else: #offset < target_3
            #move right to offset
            s.move(1,-90,0)
            time.sleep((target_3 - offset) / const)
            s.move(0,0,0)

        time.sleep(0.1)
        #move back ~2 inches
        #s.move(1,180,0)
        #time.sleep(0.26)
        #s.move(0,0,0)
        #time.sleep(0.1)
        #rotate to face zone 1
        s.move(0,0,-1)
        time.sleep(0.60)
        s.move(0,0,0)

        if dest == 2:
            time.sleep(0.1)
            #move to the right extent of zone 2
            s.move(1,-90,0)
            time.sleep(42.5 / const)
            s.move(0,0,0)

    # Move a bit closer to the line
    s.move(.7,0,0)
    time.sleep(0.45)
    s.move(0,0,0)
		



#def rot_90(s, direction):
#blah for future stuff


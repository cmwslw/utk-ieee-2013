import spine
import time

# Change this port if not on OSX
s = spine.Spine(spine.DEF_LINUX_PORT)
s.startup()

def retract_to_limit():
    if s.read_limit == True:
        return
    s.retract_rail(250)
    while s.read_limit() == False:
        pass
    s.stop_rail()

def extend_for_time(secs):
    s.extend_rail(250)
    time.sleep(secs)
    s.stop_rail()

'''
s.retract_rail(254)
time.sleep(3)
s.stop_rail()
time.sleep(0.5)
s.extend_rail(254)
time.sleep(2)
s.stop_rail()
'''
retract_to_limit()

'''
s.set_servo( "A" ,0 )
time.sleep(1)
s.set_servo("A",90)
time.sleep(1)
s.set_servo("A",180)
tmie.sleep(1)
'''
s.close()

# Global
import time
import signal
import sys

# Third-party
import serial

# Project specific
import mecanum

DEF_OSX_PORT= '/dev/tty.usbmodem12341'
DEF_LINUX_PORT= '/dev/ttyACM0'

class Spine:
    def __init__(self, port, baud=9600, t_out=1, delim='\n'):
        self.ser = serial.Serial(port, baud, timeout=t_out)
        self.delim = delim
        self.compass_tare = 0
        # Hook in SIGINT abort handler for clean aborts
        signal.signal(signal.SIGINT, self.signal_handler)
        # Read in enough bytes to clear the buffer
        self.ser.read(1000)

    def send(self, command):
        self.ser.write(command + self.delim)
        echo = self.ser.readline()
        assert echo == '> ' + command + '\r\n'
        response = self.ser.readline()
        # Be sure to chop off newline. We don't need it.
        return response[:-2]

    def ping(self):
        response = self.send('ping')
        assert response == 'ok'

    def bat(self):
        # NICK: voltage calculations should be done right here
        # the teensy command should just return the numerical ADC value.
        # This way is better because if there is a bug in the voltage
        # calculations, we will be able to fix it here rather than take
        # the bot apart and reprogram the Teensy.
	batlvl = self.send('bat')
        print(batlvl)

    def set_led(self, status):
        command = 'le ' + ('on' if status else 'off')
        response = self.send(command)
        assert response == 'ok'

    def read_compass_raw(self):
        response = self.send('rc')
        return int(response)

    def tare_compass(self):
        self.compass_tare = self.read_compass_raw()
        return self.compass_tare

    def read_compass(self):
        tared = self.read_compass_raw() - self.compass_tare
        if tared < 0:
            tared += 360
        return tared

    def average(self, n, delay, function, *args):
        l = []
        assert n > 0
        for i in range(n):
            l.append(function(*args))
            time.sleep(delay)
        if type(l[0]) == list:
            list_len = len(l[0])
            totals = [0]*list_len
            for entry in l:
                for i, item in enumerate(entry):
                    totals[i] += item
            avg = [0]*list_len
            for i, item in enumerate(totals):
                avg[i] = totals[i] / float(n)
        else:
            avg = sum(l)/float(len(l))
        return avg

    def move_for(self, delay, *args):
        self.move(*args)
        time.sleep(delay)
        self.stop()

    def read_accel(self):
        response = self.send('ra')
        ints = [int(s) for s in response.split(' ')]
        floats = [-float(i)/1000 for i in ints]
        return floats

    def read_sonar(self, sonar_id):
        assert sonar_id <= 2
        assert sonar_id >= 1
        command = 'rs ' + str(sonar_id)
        response = self.send(command)
        return int(response)

    def read_limit(self):
        command = 'lim'
        response = self.send(command)
        return not bool(int(response))

    def set_motor_speed(self, side, speed, direction):
        assert side in ['lift', 'track']
        assert 0 <= speed <= 255
        assert direction in ['cw', 'ccw']
        command = 'ld ' + side + ' ' + str(speed) + ' ' + direction
        response = self.send(command)
        assert response == 'ok'

    def set_motor_stop(self, side):
        assert side in ['lift', 'track']
        command = 'ls ' + side
        response = self.send(command)
        assert response == 'ok'

    def set_lift(self, speed, direction):
        assert direction in ['up', 'down']
        if direction == 'up':
            direction = 'cw'
        elif direction == 'down':
            direction = 'ccw'
        self.set_motor_speed('lift', speed, direction)

    def retract_rail(self, speed):
        self.set_motor_speed('track', speed, 'ccw')

    def extend_rail(self, speed):
        self.set_motor_speed('track', speed, 'cw')

    def stop_rail(self):
        self.set_motor_speed('track', 0, 'cw')

    def set_wheel(self, w_id, speed, direction):
        assert w_id in [1, 2, 3, 4]
        assert 0 <= speed <= 255
        # TODO: convert these to fw and bw
        assert direction in ['fw', 'rv']
        if w_id in [3, 4]:
            if direction == 'fw':
                direction = 'rv'
            else:
                direction = 'fw'
        # Reassign wheel numbers to accomodate them being switched
        w_id = [2,4,1,3][w_id-1]
        direction = {'fw': 'cw', 'rv': 'ccw'}[direction]
        command = 'go ' + str(w_id) + ' ' + str(speed) + ' ' + direction
        response = self.send(command)
        assert response == 'ok'

    def move(self, speed, direction, angular):
        assert 0 <= speed <= 1
        assert -180 <= direction <= 180
        assert -1 <= angular <= 1
        wheels = mecanum.move(speed, direction, angular)
        for w_id, wheel in enumerate(wheels, start=1):
            self.set_wheel(w_id, wheel[0], wheel[1])

    def stop(self):
        self.move(0, 0, 0)

    def set_servo(self, s_id, pos):
        assert s_id in ['A', 'B']
        assert 0 <= pos <= 255
        command = 'sv ' + s_id + ' ' + str(pos)
        response = self.send(command)
        assert response == 'ok'

    def grip(self):
        self.set_servo('A', 251)

    def ld_open(self):
        self.set_servo('A', 253)
        time.sleep(0.5)
        self.set_servo('A', 255)

    def ld_close(self):
        self.set_servo('A', 251)
        time.sleep(0.5)
        self.set_servo('A', 255)

    def retract_to_limit(self):
        if self.read_limit() == True:
            return
        self.retract_rail(250)
        while self.read_limit() == False:
            pass
        self.stop_rail()
    
    def extend_for_time(self, secs):
        self.extend_rail(250)
        time.sleep(secs)
        self.stop_rail()

    def startup(self):
        # Make sure we can ping the torso
        self.ping()

        # Flash LED for visual confirmation
        for i in range(3):
            self.set_led(True)
            time.sleep(0.05)
            self.set_led(False)
            # Don't waste time after the last flash
            if i != 2:
                time.sleep(0.05)

    def signal_handler(self, signal, frame):
        print '\nCleaning up after unclean exit...'
        self.stop()
        self.close()
        sys.exit(0)

    def close(self):
        self.ser.close()
        print 'Closed serial connection %s.' % self.ser.port

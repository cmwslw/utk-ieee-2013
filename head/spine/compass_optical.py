import math, time

# Be sure to 'xinput --list'
# export DISPLAY=:0
# xinput set-int-prop 13 "Device Enabled" 8 0

def to_cartesian(heading):
    if heading > 90:
        return 360 + 90 - heading
    else:
        return 90 - heading

class OpticalTracker:
    def __init__(self, m_id, s):
        self.m_id = m_id
        self.s = s
        self.mouse = file('/dev/input/mouse%d' % m_id)
        self.x = 0.0
        self.y = 0.0
        self.heading = self.s.read_compass()
        self.heading_updated = time.time()

    def update(self):
        status, dx, dy = tuple(ord(c) for c in self.mouse.read(3))
    
        def to_signed(n):
            return n - ((0x80 & n) << 1)
            
        dx = to_signed(dx)
        dy = to_signed(dy)

        # Update heading if heeded
        if (time.time() - self.heading_updated) > 0.1:
            self.heading = self.s.read_compass()
            self.heading_updated = time.time()
        
        radians = math.radians(to_cartesian(self.heading))
        print to_cartesian(self.heading)
        print radians
        self.x += dx * math.sin(radians)
        self.y += dy * math.cos(radians)

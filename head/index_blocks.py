#!/usr/bin/python

"""
index_blocks.py: Find the order and color of blocks. POC.

Usage: ./index_blocks.py blocks.jpg

Dependencies: OpenCV, python-colormath
"""

import getopt, sys
import time, datetime
import base64
import pickle
import os
from os.path import expanduser
import cv
from imaging.blocks import BlockFinder 

__author__      = "Cory Walker"
__email__       = "cwalker32@gmail.com"
__status__      = "Prototype"

start_time = time.time()
folder = datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + '/'
home = expanduser('~')

def index_blocks_spawn(drop_off, out_fn, width, height, in_fn, orig_fn):
    data = (in_fn, width, height)
    command = 'index_blocks.py --in=%s --width=%d --height=%d -p' % data
    if out_fn:
        command += ' --out=%s' % out_fn
    if orig_fn:
        command += ' --origout=%s' % orig_fn
    if drop_off:
        command += ' --dropoff'
    out = os.popen(command).read()
    bf = pickle.load(open(home+'/bf.p', 'rb'))
    #bf = index_blocks("webcam", fn, False, 320, 240, fn_base + '_orig.jpg')
    return bf

def current_blocks(drop_off, save, width=320, height=240):
    fn = None
    if save:
        base = '/home/utk-ieee/Code/ieeelog/'
        if not os.path.exists(base + folder):
            os.makedirs(base + folder)
        fn_base = base + folder + str(int((time.time() - start_time)*1000))
        fn = fn_base + '.png'
        print fn
    orig_fn = fn_base + '_orig.png'
    return index_blocks_spawn(drop_off, fn, width, height, 'webcam', orig_fn)

def index_blocks(in_image, out_image, display, width, height, drop_off=False, orig_out=None):
    # Ready set go
    if in_image == 'webcam':
        capture = cv.CaptureFromCAM(1)
        for i in range(3):
            im = cv.QueryFrame(capture)
        if orig_out:
            cv.SaveImage(orig_out, im)
    else:
        im = cv.LoadImageM(in_image, 1)
    im_resized = cv.CreateMat(height, width, cv.CV_8UC3)
    im_debug = cv.CreateMat(height, width, cv.CV_8UC3)
    #im_resized = cv.CreateMat(800, 600, cv.CV_8UC3)
    #im_debug = cv.CreateMat(800, 600, cv.CV_8UC3)
    cv.Resize(im, im_resized)
    cv.Resize(im, im_debug)
    bf = BlockFinder(im_resized, int(im_resized.rows/2), im_debug, drop_off)
    blocks = bf.get_blocks()
        
    if(out_image):
        cv.SaveImage(out_image, im_debug)

    if(display):
        cv.NamedWindow('display')
        cv.MoveWindow('display', 10, 10)
        cv.ShowImage('display', im_debug)

        # Listen for ESC key
        while True:
            c = cv.WaitKey(7) % 0x100
            if c == 27:
                break

    return bf

def main():
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hi:o:dp", ["help", "in=", "out=", "width=", "height=", "origout=", "dropoff"])
    except getopt.GetoptError as err:
        # print help information and exit:
        print str(err) # will print something like "option -a not recognized"
        usage()
        sys.exit(2)
    in_image = None
    out_image = None
    orig_out = None
    display = False
    to_pickle = False
    drop_off = False
    width = 320
    height = 240
    for o, a in opts:
        if o == "-d":
            display = True
        elif o in ("-h", "--help"):
            usage()
            sys.exit()
        elif o in ("-i", "--in"):
            in_image = a
        elif o in ("-o", "--out"):
            out_image = a
        elif o in ("--origout"):
            orig_out = a
        elif o in ("--dropoff"):
            drop_off = True
        elif o in ("--width"):
            width = int(a)
        elif o in ("--height"):
            height = int(a)
        elif o in ("-p"):
            to_pickle = True
        else:
            assert False, "unhandled option"
    
    if in_image == None:
        print "Error: you must specify an input image with --in=<IMAGE>"
        sys.exit()

    bf = index_blocks(in_image, out_image, display, width, height, drop_off, orig_out)
    if to_pickle:
        pickle.dump(bf, open(home+'/bf.p', 'wb'), protocol=2)
    else:
        block_strings = []
        for block in bf.blocks:
            block_strings.append(str(block))
        print '[' + ', '.join(block_strings) + ']'

if __name__ == "__main__":
    main()



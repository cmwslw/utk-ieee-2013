#include <Wire.h>
#include <LSM303.h>

// Globals
int ledState = HIGH;
LSM303 compass;
// Command parsing
const int MAX_ARGS = 4;
String args[MAX_ARGS];
int numArgs = 0;

// Compass Cal Values
int min_x=-302, min_y=-381, min_z=559, max_x=98, max_y=7, max_z=588;
bool calibrating=false;
LSM303::vector running_min = {2047, 2047, 2047}, running_max = {-2048, -2048, -2048};


// Pin definitions
const char LED = 6;
const char SON_PW_1 = 42;
const char SON_PW_2 = 43;
const char SON_PW_3 = 44;
const char SON_PW_4 = 45;
const char SON_RX_1 = 12;
const char SON_RX_2 = 11;
const char SON_RX_3 = 10;
const char SON_RX_4 = 9;
const char LIFT_LOAD_PWM = 27;
const char LIFT_LOAD_IN1 = 4;
const char LIFT_LOAD_IN2 = 5;
const char TRACK_LOAD_PWM = 16;
const char TRACK_LOAD_IN1 = 7;
const char TRACK_LOAD_IN2 = 8;
const char CH1_PWM = 24;
const char CH1_DIR = 20;
const char CH1_CUR = 38;
const char CH2_PWM = 25;
const char CH2_DIR = 21;
const char CH2_CUR = 39;
const char CH3_PWM = 26;
const char CH3_DIR = 22;
const char CH3_CUR = 40;
const char CH4_PWM = 14;
const char CH4_DIR = 23;
const char CH4_CUR = 41;
const char SERVO_A = 15;
const char LIM_SWITCH = 19;

void setup() {
  // Init LED pin
  pinMode(LED,OUTPUT);
  // Init sonar pins
  //pinMode(SON_PW_1,INPUT);
  //pinMode(SON_PW_2,INPUT);
  //pinMode(SON_PW_3,INPUT);
  //pinMode(SON_PW_4,INPUT);
  pinMode(SON_RX_1,OUTPUT);
  pinMode(SON_RX_2,OUTPUT);
  pinMode(SON_RX_3,OUTPUT);
  pinMode(SON_RX_4,OUTPUT);
  // Init loader driver pins
  pinMode(LIFT_LOAD_PWM,OUTPUT);
  pinMode(LIFT_LOAD_IN1,OUTPUT);
  pinMode(LIFT_LOAD_IN2,OUTPUT);
  pinMode(TRACK_LOAD_PWM,OUTPUT);
  pinMode(TRACK_LOAD_IN1,OUTPUT);
  pinMode(TRACK_LOAD_IN2,OUTPUT);
  // Init Rover 5 pins
  pinMode(CH1_PWM,OUTPUT);
  pinMode(CH1_DIR,OUTPUT);
  pinMode(CH1_CUR,INPUT);
  pinMode(CH2_PWM,OUTPUT);
  pinMode(CH2_DIR,OUTPUT);
  pinMode(CH2_CUR,INPUT);
  pinMode(CH3_PWM,OUTPUT);
  pinMode(CH3_DIR,OUTPUT);
  pinMode(CH3_CUR,INPUT);
  pinMode(CH4_PWM,OUTPUT);
  pinMode(CH4_DIR,OUTPUT);
  pinMode(CH4_CUR,INPUT);
  // Init servo pins
  pinMode(SERVO_A,OUTPUT);
  // Init limit pins
  pinMode(LIM_SWITCH, INPUT);
  digitalWrite(LIM_SWITCH, HIGH);
  
  // Init serial
  Serial.begin(9600);
  
  // Init compass
  Wire.begin();
  compass.init();
  compass.enableDefault();
  // Calibration values. Use the Calibrate example program to get the values for
  // your compass.
  compass.m_min.x = -185; compass.m_min.y = -328; compass.m_min.z = 513;
  compass.m_max.x = 232; compass.m_max.y = 232; compass.m_max.z = 563;
  
  // Display ready LED
  digitalWrite(LED,HIGH);
}

//Calibration Function for the Compass
void Cal_Compass() {
  compass.read();
  running_min.x = min(running_min.x, compass.m.x);
  running_min.y = min(running_min.y, compass.m.y);
  running_min.z = min(running_min.z, compass.m.z);

  running_max.x = max(running_max.x, compass.m.x);
  running_max.y = max(running_max.y, compass.m.y);
  running_max.z = max(running_max.z, compass.m.z);
}

/* The loop is set up in two parts. Firs the Arduino does the work it needs to
 * do for every loop, next is runs the checkInput() routine to check and act on
 * any input from the serial connection.
 */
void loop() {
  int inbyte;
  
  // Accept and parse serial input
  checkInput();
  if(calibrating==true) {Cal_Compass();}
  
}

void parse_args(String command) {
  numArgs = 0;
  int beginIdx = 0;
  int idx = command.indexOf(" ");
  
  String arg;
  char charBuffer[16];
  
  while (idx != -1)
  {
      arg = command.substring(beginIdx, idx);
  
      // add error handling for atoi:
      args[numArgs++] = arg;
      beginIdx = idx + 1;
      idx = command.indexOf(" ", beginIdx);
  }
  
  arg = command.substring(beginIdx);
  args[numArgs++] = arg;
}

/* This routine checks for any input waiting on the serial line. If any is
 * available it is read in and added to a 128 character buffer. It sends back
 * an error should the buffer overflow, and starts overwriting the buffer
 * at that point. It only reads one character per call. If it receives a
 * newline character is then runs the parseAndExecuteCommand() routine.
 */
void checkInput() {
  int inbyte;
  static char incomingBuffer[128];
  static char bufPosition=0;
  
  if(Serial.available()>0) {
    // Read only one character per call
    inbyte = Serial.read();
    if(inbyte==10) {
      // Newline detected
      incomingBuffer[bufPosition]='\0'; // NULL terminate the string
      bufPosition=0; // Prepare for next command
      
      // Supply a separate routine for parsing the command. This will
      // vary depending on the task.
      parseAndExecuteCommand(String(incomingBuffer));
    }
    else {
      incomingBuffer[bufPosition]=(char)inbyte;
      bufPosition++;
      if(bufPosition==128) {
        Serial.println("error: command overflow");
        bufPosition=0;
      }
    }
  }
}

/* This routine parses and executes any command received. It will have to be
 * rewritten for any sketch to use the appropriate commands and arguments for
 * the program you design. I find it easier to separate the input assembly
 * from parsing so that I only have to modify this function and can keep the
 * checkInput() function the same in each sketch.
 */
void parseAndExecuteCommand(String command) {
  Serial.println("> " + command);
  parse_args(command);
  if(args[0].equals(String("ping"))) {
    if(numArgs == 1) {
      Serial.println("ok");
    } else {
      Serial.println("error: usage - 'ping'");
    }
  }
  else if(args[0].equals(String("ccm"))) { //Serial port start Compass Calibration
    if(numArgs == 1) {
      min_x = 0;
      min_y = 0;
      min_z = 0;
      max_x = 0;
      max_y = 0;
      max_z = 0;
      calibrating=true;
      Serial.println("ok");
    } else {
      Serial.println("error: usage - 'ccm'");
    }
  }
  else if(args[0].equals(String("scm"))) { //Serial port stop Compass Calibration
    if(numArgs == 1) {
      calibrating=false;
      Serial.print("!M min ");
      Serial.print("X: ");
      Serial.print((int)running_min.x);
      Serial.print(" Y: ");
      Serial.print((int)running_min.y);
      Serial.print(" Z: ");
      Serial.print((int)running_min.z);

      Serial.print(" M max ");  
      Serial.print("X: ");
      Serial.print((int)running_max.x);
      Serial.print(" Y: ");
      Serial.print((int)running_max.y);
      Serial.print(" Z: ");
      Serial.println((int)running_max.z);
    } else {
      Serial.println("error: usage - 'scm'");
    }
  }
  else if(args[0].equals(String("acm"))) { //Serial port apply Compass Calibration
    if(numArgs == 1) {
      calibrating=false;
      Serial.print("!M min ");
      Serial.print("X: ");
      Serial.print((int)running_min.x);
      Serial.print(" Y: ");
      Serial.print((int)running_min.y);
      Serial.print(" Z: ");
      Serial.print((int)running_min.z);

      Serial.print(" M max ");  
      Serial.print("X: ");
      Serial.print((int)running_max.x);
      Serial.print(" Y: ");
      Serial.print((int)running_max.y);
      Serial.print(" Z: ");
      Serial.println((int)running_max.z);
      compass.m_min.x = running_min.x; compass.m_min.y = running_min.y; compass.m_min.z = running_min.z;
      compass.m_max.x = running_max.x; compass.m_max.y = running_max.y; compass.m_max.z = running_max.z;
    } else {
      Serial.println("error: usage - 'scm'");
    }
  }
  else if(args[0].equals(String("rc"))) {
    if(numArgs == 1) {
      compass.read();
      int heading = compass.heading((LSM303::vector){0,-1,0});
      Serial.println(heading);
    } else {
      Serial.println("error: usage - 'rc'");
    }
  }
  else if(args[0].equals(String("lim"))) {
    if(numArgs == 1) {
      int value = digitalRead(LIM_SWITCH);
      Serial.println(value);
    } else {
      Serial.println("error: usage - 'lim'");
    }
  }
  else if(args[0].equals(String("blk"))) {
    if(numArgs == 1) {
      int value = analogRead(SON_PW_1);
      Serial.println(value);
    } else {
      Serial.println("error: usage - 'blk'");
    }
  }
  else if(args[0].equals(String("rs"))) {
    if(numArgs == 2) {
      if(args[1].equals(String("1"))) {
        long cm = sonar_cm(SON_PW_1);
        Serial.println(cm);
      } else if(args[1].equals(String("2"))) {
        long cm = sonar_cm(SON_PW_2);
        Serial.println(cm);
      } else if(args[1].equals(String("3"))) {
        long cm = sonar_cm(SON_PW_3);
        Serial.println(cm);
      } else if(args[1].equals(String("4"))) {
        long cm = sonar_cm(SON_PW_4);
        Serial.println(cm);
      } else {
        Serial.println("error: usage - 'rs [1/2/3/4]'");
      }
    } else {
      Serial.println("error: usage - 'rs [1/2/3/4]'");
    }
  }
  else if(args[0].equals(String("ra"))) {
    if(numArgs == 1) {
      compass.read();
      Serial.print((int)compass.a.x);
      Serial.print(" ");
      Serial.print((int)compass.a.y);
      Serial.print(" ");
      Serial.println((int)compass.a.z);
    } else {
      Serial.println("error: usage - 'ra'");
    }
  }
  else if(args[0].equals(String("le"))) {
    if(numArgs == 2) {
      if(args[1].equals(String("on"))) {
        ledState = HIGH;
        digitalWrite(LED,HIGH);
        Serial.println("ok");
      } else if(args[1].equals(String("off"))) {
        ledState = LOW;
        digitalWrite(LED,LOW);
        Serial.println("ok");
      } else {
        Serial.println("error: usage - 'le [on/off]'");
      }
    } else {
      Serial.println("error: usage - 'le [on/off]'");
    }
  }
  else if(args[0].equals(String("rl"))) {
    if(numArgs == 1) {
      Serial.println(ledState);
    } else {
      Serial.println("error: usage - 'rl'");
    }
  }
  else if(args[0].equals(String("ld"))) {
    if(numArgs == 4) {
      int speed = args[2].toInt();
      boolean inPin1 = LOW;
      boolean inPin2 = HIGH;
    
      if(args[3].equals(String("ccw"))) {
        inPin1 = HIGH;
        inPin2 = LOW;
      }
    
      if(args[1].equals(String("lift"))) {
        digitalWrite(LIFT_LOAD_IN1, inPin1);
        digitalWrite(LIFT_LOAD_IN2, inPin2);
        analogWrite(LIFT_LOAD_PWM, speed);
        Serial.println("ok");
      }else if(args[1].equals(String("track"))) {
        digitalWrite(TRACK_LOAD_IN1, inPin1);
        digitalWrite(TRACK_LOAD_IN2, inPin2);
        analogWrite(TRACK_LOAD_PWM, speed);
        Serial.println("ok");
      }
    } else {
      Serial.println("error: usage - 'ld [lift/track] [speed] [cw/ccw]'");
    }
  }
  else if(args[0].equals(String("ls"))) {
    if(numArgs == 2) {
      if(args[1].equals(String("lift"))) {
        digitalWrite(LIFT_LOAD_IN1, LOW);
        digitalWrite(LIFT_LOAD_IN2, LOW);
        Serial.println("ok");
      }else if(args[1].equals(String("track"))) {
        digitalWrite(TRACK_LOAD_IN1, LOW);
        digitalWrite(TRACK_LOAD_IN2, LOW);
        Serial.println("ok");
      }
    } else {
      Serial.println("error: usage - 'ls [lift/track]'");
    }
  }
  else if(args[0].equals(String("go"))) {
    if(numArgs == 4) {
      int speed = args[2].toInt();
      
      boolean dir = LOW;
      if(args[3].equals(String("ccw"))) {
        dir = HIGH;
      }
    
      if(args[1].equals(String("1"))) {
        analogWrite(CH1_PWM, speed);
        digitalWrite(CH1_DIR, dir);
        Serial.println("ok");
      }else if(args[1].equals(String("2"))) {
        analogWrite(CH2_PWM, speed);
        digitalWrite(CH2_DIR, dir);
        Serial.println("ok");
      }else if(args[1].equals(String("3"))) {
        analogWrite(CH3_PWM, speed);
        digitalWrite(CH3_DIR, dir);
        Serial.println("ok");
      }else if(args[1].equals(String("4"))) {
        analogWrite(CH4_PWM, speed);
        digitalWrite(CH4_DIR, dir);
        Serial.println("ok");
      }
    } else {
      Serial.println("error: usage - 'go [1/2/3/4] [speed] [cw/ccw]'");
    }
  }
  else if(args[0].equals(String("sv"))) {
    if(numArgs == 3) {
      int pos = args[2].toInt();
    
      if(args[1].equals(String("A"))) {
        analogWrite(SERVO_A, pos);
        Serial.println("ok");
      }else if(args[1].equals(String("B"))) {
        Serial.println("not implemented");
      }
    } else {
      Serial.println("error: usage - 'sv [A/B] [pos]'");
    }
  }
  else {
    // Unrecognized command
    Serial.println("error: unrecognized command");
  }
}

long sonar_cm(int pin) {
  long pulse;
  pulse = pulseIn(pin, HIGH);
  return pulse / 57.87; // convert to cm
}
